import React from "react";

import {
  TextField,
  Button,
  TablePagination,
  Typography,
} from "@material-ui/core";
import axios from "axios";
import { SimpleTable } from "./CommonTable";
import { PmmBackdrop } from "./Loader";
import { useHistory } from "react-router-dom";

const usersData = async ({
  searchText,
  pageSize,
  pageIndex,
  setDataCount,
  setTableData,
  setLoader,
}) => {
  const logData = JSON.parse(localStorage.getItem("loginData"));
  const { userName } = logData.data;
  try {
    setLoader(true);
    const result = await axios.get(
      `http://e9d3e81460f1.ngrok.io/searchresults?language=${searchText}&pageSize=${pageSize}&pageIndex=${pageIndex}&userName=${userName}`
    );
    setTableData(result.data.data);
    setDataCount(result.data.count);
    setLoader(false);
  } catch (error) {
    setLoader(false);
    console.log(error);
  }
};

export const SampleTable = () => {
  const [searchText, setSearchText] = React.useState("");
  const [pageSize, setPageSize] = React.useState(10);
  const [pageIndex, setPageIndex] = React.useState(0);
  const [dataCount, setDataCount] = React.useState(0);
  const [tableData, setTableData] = React.useState([]);
  const [toggleTable, setToggleTable] = React.useState(false);
  const [loader, setLoader] = React.useState(false);
  const { push } = useHistory();

  React.useEffect(() => {
    if (searchText !== "") {
      usersData({
        searchText,
        pageSize,
        pageIndex,
        setDataCount,
        setTableData,
        setLoader,
      });
    }
  }, [toggleTable, pageSize, pageIndex]);

  const logData = JSON.parse(localStorage.getItem("loginData"));

  const handleLanguageChange = (event) => {
    setSearchText(event.target.value);
  };

  const handleClickSubmit = async () => {
    setToggleTable(!toggleTable);
  };

  const handleChangePage = async (event, newPage) => {
    setPageIndex(newPage);
  };

  const handleChangeRowsPerPage = async (event) => {
    setPageSize(+event.target.value);
    setPageIndex(0);
  };
  return (
    <>
      <Typography component="div" style={{ marginLeft: 50, marginTop: 20 }}>
        <TextField
          name="language"
          value={searchText}
          onChange={handleLanguageChange}
          label="Search by language"
        />
        <Button
          variant="contained"
          color="primary"
          onClick={handleClickSubmit}
          style={{ marginLeft: 50 }}
          disabled={searchText === ""}
        >
          Submit
        </Button>

        {logData.data.role === "Admin" ? (
          <Button
            variant="outlined"
            color="primary"
            onClick={() => push("/app/admin")}
            style={{ marginLeft: 50 }}

          >
            Tracking
          </Button>
        ) : null}
      </Typography>
      {toggleTable ? (
        <>
          <SimpleTable
            headCells={tableData.length ? Object.keys(tableData[0]) : []}
            tableData={tableData}
            idKey="id"
          />
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={dataCount}
            rowsPerPage={pageSize}
            page={pageIndex}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </>
      ) : null}
      <PmmBackdrop open={loader} />
    </>
  );
};