import React from "react";
import { useHistory } from "react-router-dom";
import { Button, Box } from "@material-ui/core";

export const Logout = () => {
  const { push } = useHistory();
  return (
    <Box display="flex" justifyContent="flex-end" m={1} p={1}>
      <Box p={1}>
        <Button variant="outlined" color="primary" onClick={() => push("/")}>
          logout
        </Button>
      </Box>
    </Box>
  );
};
