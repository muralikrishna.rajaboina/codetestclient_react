import React from "react";

import { Button, TablePagination, Box } from "@material-ui/core";
import axios from "axios";
import { SimpleTable } from "./CommonTable";
import { PmmBackdrop } from "./Loader";
import { useParams, useHistory } from "react-router-dom";

const usersData = async ({
  pageSize,
  pageIndex,
  userName,
  language,
  setDataCount,
  setTableData,
  setLoader,
}) => {
  try {
    setLoader(true);
    const result = await axios.get(
      `http://e9d3e81460f1.ngrok.io/showresults?userName=${userName}&language=${language}&pageSize=${pageSize}&pageIndex=${pageIndex}`
    );
    setLoader(false);
    setTableData(result.data.data);
    setDataCount(result.data.count);
  } catch (error) {
    setLoader(false);
    console.log(error);
  }
};

export const ShowResults = () => {
  const [pageSize, setPageSize] = React.useState(10);
  const [pageIndex, setPageIndex] = React.useState(0);
  const [dataCount, setDataCount] = React.useState(0);
  const [tableData, setTableData] = React.useState([]);
  const [loader, setLoader] = React.useState(false);
  const { userName, language } = useParams();
  const { goBack } = useHistory();

  React.useEffect(() => {
    usersData({
      pageSize,
      pageIndex,
      userName,
      language,
      setDataCount,
      setTableData,
      setLoader,
    });
  }, [pageSize, pageIndex]);

  const handleChangePage = async (event, newPage) => {
    setPageIndex(newPage);
  };

  const handleChangeRowsPerPage = async (event) => {
    setPageSize(+event.target.value);
    setPageIndex(0);
  };

  return (
    <>
      <SimpleTable
        headCells={tableData.length ? Object.keys(tableData[0]) : []}
        tableData={tableData}
        idKey="id"
      />
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={dataCount}
        rowsPerPage={pageSize}
        page={pageIndex}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />

      <Box display="flex" justifyContent="flex-end" m={1} p={1}>
        <Box p={1}>
          <Button variant="outlined" color="secondary" onClick={goBack}>
            Back
          </Button>
        </Box>
      </Box>
      <PmmBackdrop open={loader} />
    </>
  );
};
