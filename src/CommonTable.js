import {
  makeStyles,
  // Paper,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import React from "react";

const useStyles = makeStyles((_) => ({
  no_data_container: {
    height: "50px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  no_data_infographic: {
    marginRight: "30px",
  },
  no_data_text: {
    width: "386px",
  },
  header: {
    color: "rgba(0, 0, 0, 0.54)",
    letterSpacing: 0,
    padding: "8px",
  },
  cell: {
    padding: "8px",
    color: "#3F4756",
    letterSpacing: 0,
    textTransform: "capitalize",
  },
}));

export function SimpleTableHead({ headCells }) {
  const classes = useStyles();
  return (
    <TableHead>
      <TableRow>
        {headCells &&
          headCells.map((l) => (
            <TableCell key={l} className={classes.header}>
              {l.toUpperCase()}
            </TableCell>
          ))}
      </TableRow>
    </TableHead>
  );
}

export function SimpleTableBody({
  idKey,
  tableData,
  headCells,
  ActionMenu,
  onAction,
}) {
  const classes = useStyles();
  return (
    <TableBody>
      {tableData &&
        tableData.map((row) => (
          <TableRow key={row[idKey]}>
            {headCells.map((l) => (
              <TableCell className={classes.cell} key={l}>
                {row[l]}
              </TableCell>
            ))}
            <TableCell className={classes.cell} align="right">
              {ActionMenu && (
                <ActionMenu
                  onSelected={onAction && ((value) => onAction(value, row))}
                  row={row}
                />
              )}
            </TableCell>
          </TableRow>
        ))}
    </TableBody>
  );
}

export function SimpleTable({
  headCells,
  tableData,
  idKey,
  ActionMenu,
  onAction,
}) {
  const classes = useStyles();

  return (
    <Paper style={{padding:30}}>
      {tableData.length ? (
        <Table>
          <SimpleTableHead headCells={headCells} />

          <SimpleTableBody
            tableData={tableData}
            headCells={headCells}
            idKey={idKey}
            ActionMenu={ActionMenu}
            onAction={onAction}
          />
        </Table>
      ) : (
        <Typography component="div" className={`${classes.no_data_container}`}>
          <Typography component="div" className={`${classes.no_data_text}`}>
            No records found.
          </Typography>
        </Typography>
      )}
    </Paper>
  );
}
