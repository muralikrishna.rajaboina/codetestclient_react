import { Backdrop } from "@material-ui/core";
import { createStyles, makeStyles } from "@material-ui/core/styles";
import React from "react";

const useStyles = makeStyles((theme) =>
  createStyles({
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: "#fff",
    },
  })
);

export const PmmBackdrop = ({ open, onChange }) => {
  const classes = useStyles();
  return (
    <Backdrop className={classes.backdrop} open={open} onChange={onChange}>
      <div className="donut1" />
    </Backdrop>
  );
};
