import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Login } from "./Login";
import { SampleTable } from "./SampleTable";
import { AdminTable } from "./AdminTable";
import { ShowResults } from "./ShowResults";
import { Logout } from "./Logout";

export const Routes = () => (
  <Router>
    <Route exact path="/" component={Login} />
    <Route path="/app" component={Logout}/>
    <Route exact path="/app/searchResults" component={SampleTable} />
    <Route exact path="/app/admin" component={AdminTable} />
    <Route exact path="/app/admin/showResults/:userName/:language" component={ShowResults} />
  </Router>
);
