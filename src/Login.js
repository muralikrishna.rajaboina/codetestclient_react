import React from "react";
import { Formik, useFormikContext, Form } from "formik";
import * as yup from "yup";
import { TextField, Button, Grid } from "@material-ui/core";
import axios from "axios";
import { useHistory } from "react-router-dom";

const schema = yup.object().shape({
  emailId: yup.string().required().email(),
  password: yup.string().required(),
});

export const Login = () => {
  const { push } = useHistory();
  return (
    <Formik
      initialValues={{ emailId: "", password: "" }}
      validationSchema={schema}
      onSubmit={async (values) => {
        try {
          const result = await axios.post(
            `http://e9d3e81460f1.ngrok.io/users/signIn`,
            { emailId: values.emailId, password: values.password }
          );
          localStorage.setItem("loginData", JSON.stringify(result));
          push("/app/searchResults");
        } catch (error) {
          console.log(error);
        }
      }}
      component={LoginError}
    />
  );
};

const LoginError = () => {
  const {
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
    isSubmitting,
  } = useFormikContext();

  return (
    <Form onSubmit={handleSubmit} >
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        justify="center"
        style={{ marginTop: 50 }}
      >
        <Grid item xs={3}>
          <TextField
            name="emailId"
            type="email"
            label="Enter Email ID"
            onChange={handleChange}
            onBlur={handleBlur}
            placeholder="email"
            error={errors.emailId && touched.emailId && errors.emailId}
            helperText={
              errors.emailId && touched.emailId ? errors.emailId : undefined
            }
          />
        </Grid>

        <Grid item xs={3}>
          <TextField
            name="password"
            type="password"
            label="Enter Password"
            placeholder="Password"
            onChange={handleChange}
            onBlur={handleBlur}
            error={errors.password && touched.password && errors.password}
            helperText={
              errors.password && touched.password ? errors.password : undefined
            }
          />
        </Grid>
        <Grid style={{ marginTop: 15 }}>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            disabled={isSubmitting}
          >
            Submit
          </Button>
        </Grid>
      </Grid>
    </Form>
  );
};
